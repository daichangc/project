import pygame
import random

pygame.init()
screen = pygame.display.set_mode((500, 600))
pygame.display.set_caption("动物翻翻看")
clock = pygame.time.Clock()
pygame.time.set_timer(pygame.USEREVENT + 1, 1000)  # 设置定时事件
fo_name = pygame.font.match_font('fangsong')  # 改字体
cd_font = pygame.font.Font(fo_name, 30)  # 倒计时字体
running = True

state = 'a'  # 状态
tim = 90  # 时间
score = 0  # 分数
it = True  # 初始状态
de = False  # 得分判断
li_s = []  # 关卡随机卡牌数量容器
a1 = a2 = b1 = b2 = wait = None  # 卡1下标 卡1的值 卡2下标 卡2的值 等待


def am(dat):  # 把图片实例化
    return pygame.image.load(f'{dat}.png').convert()


def s_start():  # a状态，主页图，三个按键的位置创建
    zhu = pygame.image.load('z.jpg').convert()
    screen.blit(zhu, (0, 0))

    le1 = pygame.Rect(153, 300, 190, 60)
    le2 = pygame.Rect(153, 385, 190, 60)
    le3 = pygame.Rect(153, 470, 190, 60)

    return le1, le2, le3


animal = ['cat', 'dog', 'cattle', 'dragon', 'panda', 'rabbit', 'ss', 'lh', 'l', 'ji']  # 所有动物

pai_ls = ['back' for _ in range(6)]  # 初始卡牌，要显示的卡牌表
pai_am = [0, 0, 0, 0, 0, 0]  # 以后小动物要生成的牌表
fzy = pygame.Rect(190, 330, 120, 30)
xxx = pygame.Rect(190, 370, 120, 30)
yse_no_re = pygame.Rect(200, 200, 30, 50)  # 对错的时候显示图标
pai0_re = pygame.Rect(50, 50, 100, 150)  # 1-6张卡牌位置创建
pai1_re = pygame.Rect(200, 50, 100, 150)
pai2_re = pygame.Rect(350, 50, 100, 150)
pai3_re = pygame.Rect(50, 250, 100, 150)
pai4_re = pygame.Rect(200, 250, 100, 150)
pai5_re = pygame.Rect(350, 250, 100, 150)
pai_rect = [pai0_re, pai1_re, pai2_re, pai3_re, pai4_re, pai5_re]


def lo():  # 显示卡牌
    global a1, a2, b1, b2, tim, wait, score, de, li_s, pai_am
    for i, v in enumerate(pai_rect):
        screen.blit(am(pai_ls[i]), v)

        if b2:
            if not wait:
                wait = tim
                pygame.event.set_blocked(pygame.MOUSEBUTTONDOWN)  # 鼠标失效
                de = True
            else:
                if wait - tim > 1:
                    pai_ls[a1] = 'back'
                    pai_ls[b1] = 'back'
                    a1 = a2 = b1 = b2 = wait = None
                    pygame.event.set_allowed(pygame.MOUSEBUTTONDOWN)  # 鼠标恢复

                if a2 == b2 and a2:  # a2和b2可能都为None
                    screen.blit(am("对"), yse_no_re)
                    screen.blit(am('fen'), (150, 430))
                    if de:
                        score += 1
                    # 相同的制0
                    pai_am[a1] = 0
                    pai_am[b1] = 0
                    pai_am = [i for i in pai_am if i != 0]  # 把不是0的留下
                    if len(set(pai_am)) == 4:               # 如有4不同，先随机一个，如有5不同则在5中选。
                        pai_am.insert(a1, random.choice(li_s))
                        pai_am.insert(b1, random.choice(pai_am) if len(set(pai_am)) == 5 else random.choice(li_s))
                    else:
                        pai_am.insert(a1, random.choice(li_s))
                        pai_am.insert(b1, random.choice(li_s))

                if a2 != b2 and a2:
                    screen.blit(am("错"), yse_no_re)
                de = False


def le():  # 处理卡牌
    global it, a1, a2, b1, b2, li_s
    if it:  # 初始化卡牌
        li_s = random.sample(animal, n)  # 随机抽取6，8，10张照片
        for i in range(5):  # 前5张随机生成
            pai_am[i] = random.choice(li_s)
        pai_am[5] = random.choice(pai_am[:5]) if len(set(pai_am[:5])) == 5 else random.choice(li_s)  # 5张不同，在5张选；否随机。
        it = False

    for j, v in enumerate(pai_rect):  # 点击卡牌位置
        if v.collidepoint(pygame.mouse.get_pos()):
            pai_ls[j] = pai_am[j]  # 点击处 动物表索引 赋值 显示表索引
            if not a2:   # 如
                a1 = j
                a2 = pai_am[j]
            elif a1 != j:   # 如点击的不是同一张则赋值第二张
                b1 = j
                b2 = pai_am[j]


def s_process():  # b状态
    lo()  # 显示卡牌
    # 倒计时
    tis = cd_font.render(str(f'倒计时:{tim}'), True, (170, 15, 15))
    screen.blit(tis, (0, 400))
    ff = cd_font.render(str(f'当前得分:{score}'), True, (170, 15, 15))
    screen.blit(ff, (0, 450))
    ff = cd_font.render(str(f'目标得分:{reach}'), True, (170, 15, 15))
    screen.blit(ff, (0, 500))


def s_end():
    screen.blit(cd_font.render(str('游戏结束'), True, (170, 15, 15)), (190, 250))

    if score >= reach:
        screen.blit(cd_font.render(str('挑战成功'), True, (170, 15, 15)), (190, 210))
    else:
        screen.blit(cd_font.render(str('挑战失败'), True, (170, 15, 15)), (190, 210))

    screen.blit(cd_font.render(str(f'最终得分{score}分'), True, (170, 15, 15)), (170, 290))
    screen.blit(cd_font.render(str(f'返回主页'), True, (170, 15, 15)), fzy)
    screen.blit(cd_font.render(str(f'退出游戏'), True, (170, 15, 15)), xxx)
    pygame.event.set_allowed(pygame.MOUSEBUTTONDOWN)  # 鼠标恢复


while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        if event.type == pygame.MOUSEBUTTONDOWN:
            if state == 'b':  # b状态
                le()
            if state == 'a':  # a状态选择难度，进入状态b, n张图片类型
                level = s_start()
                if level[0].collidepoint(pygame.mouse.get_pos()):
                    state, n, tim, reach = 'b', 6, 3, 15
                if level[1].collidepoint(pygame.mouse.get_pos()):
                    state, n, tim, reach = 'b', 8, 110, 15
                if level[2].collidepoint(pygame.mouse.get_pos()):
                    state, n, tim, reach = 'b', 10, 80, 15
            print('点了一下')
            if state == 'c':
                if fzy.collidepoint(pygame.mouse.get_pos()):
                    state = 'a'
                if xxx.collidepoint(pygame.mouse.get_pos()):
                    running = False

        if event.type == pygame.USEREVENT + 1 and state == 'b':  # 检测定时器事件
            tim -= 1
            if tim == -1:
                state = 'c'
                pai_ls = ['back' for _ in range(6)]  # 把牌全部翻回去
                a2 = None
                it = True   # 初始化打开

        # 背景
        screen.fill((136, 138, 138))
        # 开始，过程，结束
        if state == 'a':
            s_start()

        elif state == 'b':
            s_process()

        elif state == 'c':
            s_end()

        pygame.display.flip()
        clock.tick(60)