import requests
from lxml import etree
import re

url = 'https://www.tianqi24.com/meilan/'
headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36'
}
response = requests.get(url=url, headers=headers)
s = response.text
h = etree.HTML(s)
div = h.xpath('//section/ul[@class="col5"]/li')
lis = []
for j in div[1:]:
    d = etree.tostring(j, encoding='utf-8').decode('utf-8')
    ss = ''
    for i in d:
        if i != ' ' and i != '\n':
            ss += i
    tian = re.findall('<div>(.*?)</div>', ss)
    del tian[1]
    lis.append(tian)
print(lis)
